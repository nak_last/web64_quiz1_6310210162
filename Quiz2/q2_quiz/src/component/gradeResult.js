function GradeResult(props) {
    return (
        <div>
            <h3> Name: {props.name} </h3>
            <h3> Points:{props.point} </h3>
            <h3> Grade: {props.grade}</h3>
        </div>
    );
}

export default GradeResult;