import { useState } from "react";
import { Button, Typography, Box, Container } from "@mui/material";
import Grid from "@mui/material/Grid";
import GradeResult from '../component/gradeResult';

function Calculator() {

    const [ name, setName ] = useState("");
    const [ point, setPoint ] = useState("");
    const [ grade, setGrade ] = useState("");

    function CalculatorGrade() {
        let p = parseInt(point);

        if(p > 80 ){
            setGrade("A");
        }else if(p > 70){
            setGrade("B");
        }else if(p > 50){
            setGrade("C")
        }else if(p > 40){
            setGrade("D")
        }else {
            setGrade("E");
        }
    }

    return (
        <Container sx={{ width: "80%", textAlign: "center" }}>
            <Grid container spacing={2} sx={{ marginTop: '10px' }}>
                <Grid item xs={12}>
                    <Box sx={{ textAlign: "center"}}>
                        <Typography variant="h5"> 
                            Calculate Grade
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{ textAlign: "center"}}>
                        Name: <input type="text"
                                    value={ name } 
                                    onChange={ (e) => { setName (e.target.value); } } /> 
                                    <br />
                                    <br />
                        Points: <input type="text" 
                                    value={ point } 
                                    onChange={ (e) => { setPoint (e.target.value); } } /> 
                                    <br />
                                    <br />
                        <br />
                        <Button variant="contained" disableElevation onClick={ () => { CalculatorGrade() } }>Calculate</Button>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{ textAlign: "center" }}>
                        {
                            grade != 0 && 
                           <div>
                                Grade
                               <GradeResult
                                    name={ name } 
                                    point={ point }
                                    grade={ grade } />
                            </div>
                        }
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}export default Calculator;