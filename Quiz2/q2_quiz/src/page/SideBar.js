import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import { Box, Drawer, ListItem, ListItemText, ListItemIcon, Typography } from '@mui/material';
import DescriptionIcon from '@mui/icons-material/Description';
import CalculateIcon from '@mui/icons-material/Calculate';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Divider from '@mui/material/Divider';

import { Link } from 'react-router-dom';

function SideBar({children}) {
    return(
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position='fixed' sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                <Toolbar>
                    <Typography variant='h6' noWarp component='div'>
                        Calculate Grade
                    </Typography>
                </Toolbar>
            </AppBar>

            <Drawer
             variant='permanent'
             sx={{
                 width: 240,
                 flexShrink: 0,
                 [`& .MuiDrawer-paper`]: { width: 240, boxSizing: 'border-box'},
             }}
            >
                <Toolbar />
                <Box sx={{ overflow: 'auto' }}>
                    <List>
                        {['About'].map((text, index) => (
                        <ListItem button key={text} component={ Link } to='/' sx={{ my: 1 }}>
                            <ListItemIcon>
                                {<DescriptionIcon />}
                            </ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                        ))}
                        {['Calcualte grade'].map((text, index) => (
                        <ListItem button key={text} component={ Link } to='Cal' sx={{ my: 1 }}>
                            <ListItemIcon>
                                {<CalculateIcon />}
                            </ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                        ))}
                    </List>
                    <Divider sx={{ mt: 89 }} />
                </Box>
            </Drawer>
            <Box component='main' sx={{ flexGrow: 1, p: 3 }}>
                <Toolbar />
                    <Typography>
                        {children}
                    </Typography>
            </Box>
        </Box>
    );
}

export default SideBar;