import { Box, Container, Grid, Typography } from "@mui/material";


function About(props) {

    return (
        <Container sx={{ width: "80%" }} textAlign='center'>
             <Grid container spacing={2} sx={{ marginTop: '10px' }}>
                <Grid item xs={12}>
                    <Box sx={{ textAlign: "center"}}>
                        <Typography variant="h5"> 
                            Crate by 6310210162
                        </Typography>
                        <Typography variant="h5"> 
                            Content: 080-XXXXXXXX
                        </Typography>
                        <Typography variant="h5"> 
                            Address: Trang
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default About;
