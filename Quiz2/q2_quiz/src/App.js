import About from './page/About';
import Calculator from './page/Calculator';
import SideBar from './page/SideBar';

import './App.css';
import { Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div>
      <SideBar />
      <Routes>
        <Route path='/' element={ <About /> } />
        <Route path='Cal' element={ <Calculator /> } />
      </Routes>
    </div>
  );
}

export default App;